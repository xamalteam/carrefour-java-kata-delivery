# Ce fut un test technique de WE-IT à réaliser en 3 jours.

# Carrefour Java kata -- Delivery

> Vous trouverez ici un exercice assez libre d'implémentation à réaliser dans un temps restreint.\
> Il a pour objectif d'ouvrir la discussion lors d'un futur entretien.\
> Nous vous conseillons de ne pas y passer plus de 2h même si vous n'avez pas eu le temps de finir les User Stories obligatoires. 

## Consignes
L'exercice est composé d'une partie obligatoire, le [Minimum Valuable Product](#mvp).\
Il y a également des [fonctionnalités facultatives](#features-bonus) afin d'utiliser le temps restant pour vous démarquer.\
Les stories ne possèdent pas de critères d'acceptance, c'est à vous de les définir après votre analyse fonctionnelle de la story.

**S'il vous manque une information, faites un choix et restez cohérent avec celui-ci.**

### Contraintes
- Spring-boot 3.2.x
- Java 21
- Git
- Fichier `README.md` -- _Explique les potentielles subtilités de votre implémentation et comment lancer votre projet_.

### Livraison
Le code devrait être disponible sur un repository [GitLab](https://gitlab.com).

### Evaluation
**Il n'y a pas de "bonne" façon de réaliser cet exercice.**\
Nous sommes intéressés par vos choix d'implémentations, votre technique, l'architecture du code et le respect des contraintes.\
_Faites également attention à la taille de vos commits et leurs messages._

### Tips
Pour créer rapidement votre base de projet, utilisez [spring initializr](https://start.spring.io/)

## Exercice
### MVP
#### User Story
> En tant que client, je peux choisir mon mode de livraison.\
> Les modes de livraison disponibles sont : `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`.

#### User Story
> En tant que client, je peux choisir mon jour et mon créneau horaire.\
> Les créneaux sont spécifiques au mode de livraison et réservable par d'autres clients.

### Features Bonus
Les fonctionnalités suivantes sont optionnelles et non exhaustives.\
Elles n'ont pas de priorité entre elles, vous pouvez implémenter celles qui vous intéressent ou en proposer d'autres.

#### API REST
- Proposer une API REST consommable via http pour interagir avec les services réalisés dans le MVP
- Implémenter les principes HATEOAS dans votre API REST
- Sécuriser l'API
- Utiliser une solution non-bloquante
- Documenter l'API REST

#### Persistence
- Proposer une solution de persistence des données
- Proposer une solution de cache

#### Stream
- Proposer une solution de streaming de données
- Proposer une solution de consommation et/ou production d'évènements

### CI/CD
- Proposer un system de CI/CD pour le projet
- Proposer des tests End to End à destination de votre application

### Packaging
- Créer un container de votre application
- Déployer votre application dans un pod
- Créer une image native de votre application

# **Réalisation**


## Expliacation des potentielles subtilités de notre implémentation et comment lancer le projet.

### Explications des subtilités de notre implémentation

Notre implémentation présente plusieurs subtilités qui méritent d'être soulignées :

1. **L'utilisation des DTOS** : 
J'ai choisi d'utiliser les DTOs pour définir des structures de données adaptées aux besoins de la couche de présentation. Par exemple, dans le cas de l'entité Client, même si le mot de passe est encrypté dans la base de données, j'ai pris la décision d'omettre le champ motDePasse dans la classe ClientDTO.

2. **L'utilisation des Exceptions handlers** : 
J'ai intégré des gestionnaires d'exceptions pour personnaliser les erreurs et fournir des messages plus clairs et explicites aux consommateurs de l'API REST.

3. **Intégration des Validations** : 
J'ai mis en place des validateurs personnalisés afin de renforcer la sécurité de l'API REST et de prévenir la manipulation de données incorrectes.

### Comment lancer le projet

Voici les étapes pour télécharger et lancer une application Spring Boot à partir d'un repository GitLab en format Markdown :


1. **Cloner le repository GitLab** :
   Utilisez la commande `git clone` pour cloner le repository GitLab sur votre machine locale.

   ```
   git clone <https://gitlab.com/xamalteam/carrefour-java-kata-delivery.git>
   ```

2. **Importer le projet dans votre IDE** :
   Ouvrez votre IDE (Eclipse, IntelliJ IDEA, etc.) et importez le projet Spring Boot que vous avez cloné. Assurez-vous que votre IDE est configuré pour supporter Java 21.

3. **Configurer les propriétés de l'application** :
   Assurez-vous que les propriétés de l'application dans le fichier `application.properties` sont correctement configurées pour votre environnement.

4. **Compiler l'application** :
   Utilisez les outils de compilation de votre IDE ou bien Maven pour compiler l'application Spring Boot.
   vous pouvez exécuter la commande suivante à la racine du projet :

   ```
   mvn clean install
   ```

5. **lancer zookeeper** :

	```
	.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties
	```

6. **lancer kafka** :

	```
	.\bin\windows\kafka-server-start.bat .\config\server.properties
	
	```
7. **Lancer l'application** :
   Vous pouvez lancer l'application Spring Boot directement à partir de votre IDE en exécutant la classe principale (`@SpringBootApplication`) 
   ou bien en utilisant Maven pour exécuter l'application. Avec Maven, utilisez la commande :

   ```
   mvn spring-boot:run
   ```

8. **Accéder à l'application** :
   Une fois l'application démarrée, vous pouvez accéder à celle-ci via l'URL spécifiée dans le fichier de configuration (`http://localhost:8080`).
   
   **documentation du REST API :**
   
   ```
    http://localhost:8080/swagger-ui/index.html
   ```



## Réalisations

**️✔** Fait  
**x** A faire

### MVP

#### User Story **️✔** 

En tant que client, je peux choisir mon mode de livraison.\
Les modes de livraison disponibles sont : DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP.

#### User Story **️✔** 

En tant que client, je peux choisir mon jour et mon créneau horaire.\
Les créneaux sont spécifiques au mode de livraison et réservable par d'autres clients.


### Features Bonus

#### API REST
- Proposer une API REST consommable via http pour interagir avec les services réalisés dans le MVP **️✔** 
- Implémenter les principes HATEOAS dans votre API REST **x**
- Sécuriser l'API **️✔** 
- Implémenter les validators **️✔** 
- Implémenter les exceptions handlers **️✔** 
- Utiliser une solution non-bloquante **x**
- Documenter l'API REST **️✔** 

#### Persistence
- Proposer une solution de persistence des données **️✔** 
- Proposer une solution de cache **x**

#### Stream
- Proposer une solution de streaming de données **️✔** 
- Proposer une solution de consommation et/ou production d'évènements **️✔** 

### CI/CD
- Proposer un system de CI/CD pour le projet **x**
- Proposer des tests End to End à destination de votre application **x**

### Packaging
- Créer un container de votre application **️✔**
- Déployer votre application dans un pod **x**
- Créer une image native de votre application **x**