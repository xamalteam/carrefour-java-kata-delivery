package co.weit.testKata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.weit.testKata.entities.Livraison;

@Repository
public interface LivraisonRepository extends JpaRepository<Livraison, Integer> {
}
