package co.weit.testKata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestKataApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestKataApplication.class, args);
	}

}
