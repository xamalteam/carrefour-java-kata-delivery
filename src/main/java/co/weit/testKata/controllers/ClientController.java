package co.weit.testKata.controllers;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;

import co.weit.testKata.DTO.ClientDTO;
import co.weit.testKata.DTO.LivraisonDTO;
import co.weit.testKata.entities.Client;
import co.weit.testKata.entities.Livraison;
import co.weit.testKata.exception.ErrorCodes;
import co.weit.testKata.exception.InvalidEntityException;
import co.weit.testKata.services.ClientService;
import co.weit.testKata.services.LivraisonService;
import co.weit.testKata.validators.ClientValidator;
import co.weit.testKata.validators.LivraisonValidator;
import co.weit.testKata.utils.Constants;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(Constants.CLIENT_ENDPOINT)
@Validated
public class ClientController {

    

    private final Logger log = LoggerFactory.getLogger(Client.class);
    
    private ClientService clientService;
    private LivraisonService livraisonService;

    public ClientController(ClientService clientService,LivraisonService livraisonService){
        this.clientService = clientService;
        this.livraisonService = livraisonService;
    }

    @GetMapping
    public ResponseEntity<List<ClientDTO>> getAllClients() {
        List<ClientDTO> clientDTOs = clientService.getAllClients().stream().map(ClientDTO::fromEntity ).collect(Collectors.toList());
        return new ResponseEntity<>(clientDTOs, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientDTO> getClientById(@PathVariable @Min(1) Integer id) {
        ClientDTO clientDTO = ClientDTO.fromEntity(clientService.getClientById(id));
        return new ResponseEntity<>(clientDTO, HttpStatus.OK);
    }

    @PostMapping("/chooseNiche")
    public ResponseEntity<LivraisonDTO>  chooseNiche(@RequestBody LivraisonDTO livraisonDTO) {
        
        List<String> errors = LivraisonValidator.validate(livraisonDTO);
        if (!errors.isEmpty()) {
        log.error("Livraison invalide {}", livraisonDTO);
        throw new InvalidEntityException("Livraison invalide", ErrorCodes.LIVRAISON_NOT_VALID, errors);
        }

        livraisonDTO.setIdClient(livraisonDTO.getIdClient());
        Livraison livraison = LivraisonDTO.toEntity(livraisonDTO);
        Client clientToSave = clientService.getClientById(livraisonDTO.getIdClient());
        livraison.setClient(clientToSave);
        livraisonService.saveLivraison(livraison);
        return new ResponseEntity<>(livraisonDTO, HttpStatus.CREATED);
    }

    @PostMapping
    public ResponseEntity<ClientDTO> addClient(@Valid @RequestBody Client client) {
        
        ClientDTO clientDTO = ClientDTO.fromEntity(client);
        log.debug("REST request to save client : {}", clientDTO);
        List<String> errors = ClientValidator.validate(clientDTO);
        if (!errors.isEmpty()) {
        log.error("Client invalide {}", clientDTO);
        throw new InvalidEntityException("Client invalide", ErrorCodes.CLIENT_NOT_VALID, errors);
        }
        clientService.saveClient(client);
        return new ResponseEntity<>(clientDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable @Min(1) Integer id) {
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}