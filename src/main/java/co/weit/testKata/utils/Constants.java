package co.weit.testKata.utils;

public class Constants {

  public static final String APP_ROOT = "/api/v1";

  public static final String CLIENT_ENDPOINT = APP_ROOT + "/clients";
  public static final String LIVRAISON_ENDPOINT = APP_ROOT + "/livraisons";
  public static final String AUTH_ENDPOINT = APP_ROOT +"/auth";
  public static final String KAFKA_ENDPOINT = APP_ROOT +"/kafka";
  
}
