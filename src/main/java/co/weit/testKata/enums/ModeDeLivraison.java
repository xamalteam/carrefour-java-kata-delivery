package co.weit.testKata.enums;

public enum ModeDeLivraison {
    DRIVE,
    DELIVERY, 
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
