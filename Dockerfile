FROM openjdk:21-jdk-slim
ADD target/carrefour-java-kata-delivery.jar carrefour-java-kata-delivery.jar
EXPOSE 8080
CMD ["java","-jar","/carrefour-java-kata-delivery.jar"]